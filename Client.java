/* 
Based on COMP335 Distributed System Simulation Project
TEAM MEMBERS: Kelvin Liu, Julian Cesaro, Lachlan McCormack

*/

import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    // Declare socket, input stream, and output stream
    private Socket socket = null;
    private InputStream is = null;
    private DataOutputStream out = null;

    // System Server Information
    String serverInfo = "";
    String[] servers = new String[7];
    int jobID = 0;
    //public static ArrayList<Server> serverList = new ArrayList<Server>();

    // String that will hold messages and message buffer
    String line = "";
    byte[] msg = null;

    // Client constructor: creates socket and authenticates
    public Client(String address, int port) {
        createSocket(address, port);
    }

    // Creates the socket connection and output channel
    public void createSocket(String address, int port) {
        try {
            this.socket = new Socket(address, port);

            System.out.println("Connected");

            this.out = new DataOutputStream(this.socket.getOutputStream());
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    // Function to receive messages from the server
    // Returns: string
    public String receiveMsg() {
        try {
            is = socket.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String message = br.readLine();
            return message;
        } catch (IOException i) {
            System.out.println(i);
            return null;
        }
    }

    public void sendMsg(String m) {
        try {
            line = m + "\n";
            msg = line.getBytes();
            out.write(msg);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void authenticate() {
        String user = System.getProperty("user.name");

        // Create a message and send to server
        sendMsg("HELO");

        // Receive message from server
        line = receiveMsg();

        sendMsg("AUTH " + user);

        line = receiveMsg();

        //readXML("/home/comp335/comp335/ds-sim/system.xml");
    }

    public boolean fits(String[] server, String[] job) {
        //job [3] = time, [4] = cores, [5] = mem, [6] = disk
        //server [3] = time, [4] = cores, [5] = mem, [6] = disk


        //check if all metrics fit.
        for (int i = 4; i < job.length - 1; i++) {
			if (Integer.valueOf(job[i]) > Integer.valueOf(server[i])) {

				return false;
			}

		}
        return true;

    }


	class sortByCores implements Comparator<String[]> {
		public int compare(String[] a, String[] b) {
			//servers[i][4] is the number of cores of server i
			return Integer.valueOf(a[4]) - Integer.valueOf(b[4]);
		}

	}



    /*
    The splitSearchFirstFit Algorithms below is based on the firstFit implementation by fellow Group 14 member Kelvin Liu for Stage 2 of the project.

    */


	public void splitSearchFirstFit() {
        String jobInfo = "";
        String[] job = new String[7];
        int avgNoCore = 0;

        ArrayList<String[]> servers = new ArrayList<String[]>();
        ArrayList<String[]> firstServ = new ArrayList<String[]>();
        
        boolean sorted = false;
        boolean firstRun = false;
        boolean schd = false;

        while (!line.equals("QUIT\n")) {
            //Signal ready for job
            sendMsg("REDY");
            
            //receive message from server
            jobInfo = receiveMsg();

            //Split up job info
            job = jobInfo.split(" ");

            if (!jobInfo.equals("NONE")) {

                sendMsg("RESC All");
                
                ArrayList<String[]> serverUpdate = new ArrayList<String[]>();
                serverInfo = receiveMsg();
                
                //Flag to indicate whether scheduled yet or not
                schd = false;

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();
                    
                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {
                        serverUpdate.add(serverInfo.split(" "));
                    }
                    if (serverInfo.equals(".")) {
                        break;
                    }
                }

                //Sort servers by cores if first run through;
                if (sorted == false) {

                    //Sort the serverUpdate arraylist of servers, now a full list.
                    Collections.sort(serverUpdate, new sortByCores());
                    
                    //Is now sorted
                    sorted = true;

                    servers = serverUpdate;

                    int coreTotal = 0;
                    for(int i = 0; i < servers.size(); i++)
                    {
                        coreTotal += Integer.parseInt(servers.get(i)[4]);

                    }
                    avgNoCore = (coreTotal/servers.size())+1;
                    System.out.println(avgNoCore);
                    //Put all of the servers in firstServ, now sorted
                    if(firstRun == false){
                        for(int i = 0; i < servers.size(); i++){
                            firstServ.add(servers.get(i));
                        }
                    }
                    //First run is true now because it's happened so it won't happen again.
                    firstRun = true;
                } else {

                    //Update servers
                    for (int i = 0; i < servers.size(); i++) {
                        for (int x = 0; i < serverUpdate.size(); x++) {
                            if (servers.get(i)[0].equals(serverUpdate.get(x)[0]) && servers.get(i)[1].equals(serverUpdate.get(x)[1])) {
                                String[] curr = serverUpdate.get(x);
                                servers.set(i, curr);
                                break;
                            }
                        }
                    }
                }             
                
                //If the job cores is less than the average across all servers
                if(Integer.parseInt(job[4]) < avgNoCore){
                    for (int k = (int)(servers.size()/2); k > 0; k--) {
                        if (fits(servers.get(k), job)) {
                            //if fit send SCHD message
                            sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                            receiveMsg();
                            schd = true;
                            break;
                        }
                    }
                }

                //The above did not find match, so search other half of search space
                if(schd == false){
                    for (int k = (int)(servers.size()/2); k < servers.size(); k++) {
                        if (fits(servers.get(k), job)) {
                            //if fit send SCHD message 
                            sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                            receiveMsg();
                            schd = true;
                            break;
                        }
                    }
                }
                

                // Above has failed to find match. Schedule to first fit from initial servers
                if(schd == false){
                    for (int k = 0; k < firstServ.size(); k++) {
                        if (fits(firstServ.get(k), job) && Integer.parseInt(servers.get(k)[2]) == 3) {
                            //if fit send SCHD message
                            sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                            receiveMsg();
                            break;
                        }
                    }
                }
            } else {
                close();
            }
        }
    }

    /*
        The firstFit Algorithms below was implemented by fellow Group 14 member Kelvin Liu for Stage 2 of the project.
        All credit for the below algorithm goes to him.

        It has been included here to allow for side-by-side testing and comparison in one project.
    */
    public void firstFit() {
        String jobInfo = "", worstType = "", altType = "";
        String[] job = new String[7];

        ArrayList<String[]> servers = new ArrayList<String[]>();
        ArrayList<String[]> firstServ = new ArrayList<String[]>();
        boolean sorted = false, firstRun = false, schd = false;

        while (!line.equals("QUIT\n")) {
            sendMsg("REDY");
            //receive message from server
            jobInfo = receiveMsg();
            job = jobInfo.split(" ");

            if (!jobInfo.equals("NONE")) {

                sendMsg("RESC All");
                // ArrayList<String[]> servers = new ArrayList<String[]>();
                ArrayList<String[]> serverUpdate = new ArrayList<String[]>();
                serverInfo = receiveMsg();
                schd = false;

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();
                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {
                        serverUpdate.add(serverInfo.split(" "));
                    }
                    if (serverInfo.equals(".")) {
                        break;
                    }
                }

                //Sort servers by cores if first run through;
                if (sorted == false) {
                    Collections.sort(serverUpdate, new sortByCores());
                    sorted = true;
                    servers = serverUpdate;
                    if(firstRun == false){
                        for(int i = 0; i < servers.size(); i++){
                            firstServ.add(servers.get(i));
                        }
                    }
                    firstRun = true;
                } else {
                    //Update servers
                    for (int i = 0; i < servers.size(); i++) {
                        for (int x = 0; i < serverUpdate.size(); x++) {
                            if (servers.get(i)[0].equals(serverUpdate.get(x)[0]) && servers.get(i)[1].equals(serverUpdate.get(x)[1])) {
                                String[] curr = serverUpdate.get(x);
                                servers.set(i, curr);
                                break;
                            }
                        }
                    }
                }

                //go through arraylist of servers and check for fitness.
                for (int k = 0; k < servers.size(); k++) {
                    if (fits(servers.get(k), job)) {
                        //if fit send SCHD message
                        sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                        receiveMsg();
                        schd = true;
                        break;
                    }
                }
                if(schd == false){
                    for (int k = 0; k < firstServ.size(); k++) {
                        if (fits(firstServ.get(k), job) && Integer.parseInt(servers.get(k)[2]) == 3) {
                            //if fit send SCHD message
                            sendMsg("SCHD " + job[2] + " " + servers.get(k)[0] + " " + servers.get(k)[1]);
                            receiveMsg();
                            break;
                        }
                    }
                }
            } else {
                close();
            }
        }
    }

    // Main loop of the Client
    public void allToLargest() {

        boolean largestServerCheck = false;
        String largestServerType = "";
        int largestCoreCount = 0;

        while (!line.equals("QUIT\n")) {

            sendMsg("REDY");
            //receive message from server
            line = receiveMsg();
            System.out.println(line);

            if (largestServerCheck == false) {

                sendMsg("RESC All");

                while (!serverInfo.equals(".")) {

                    sendMsg("OK");

                    serverInfo = receiveMsg();

                    //Only add server info to array of server if it is not "." or "DATA"
                    if (!serverInfo.equals(".") && !serverInfo.equals("DATA")) {

                        servers = serverInfo.split(" ");

                        //System.out.println(servers[2]);

                        //If current coreCount is greater than largest coreCount
                        //Make current server the largestServer
                        //Server type is at servers[0] and core count is at servers[4]
                        if (Integer.parseInt(servers[4]) > largestCoreCount) {
                            largestCoreCount = Integer.parseInt(servers[4]);
                            largestServerType = servers[0];
                        }
                    }
                    if (serverInfo.equals(".")) {
                        break;
                    }
                }
                largestServerCheck = true;
            }
            if (!line.equals("NONE")) {

                sendMsg("SCHD " + jobID + " " + largestServerType + " 0");

                jobID++;

                //receive message from server
                line = receiveMsg();

            } else if (line.equals("NONE")) {

                close();
            }
        }
    }


    // public void readXML(String file){
    //     DOMParser p = new DOMParser(file);

    //     serverList = p.serverArr;
    // }

    // Function to terminate connection
    public void close() {
        sendMsg("QUIT");
    }

    public static void main(String args[]) {
        Client client = new Client("127.0.0.1", 8096);
        client.authenticate();
        if(args.length > 1) {
            if (args[0].equals("-a")) {
                if (args[1].equals("ssff")) {
                    //use first fit
					client.splitSearchFirstFit();;
                    System.out.println("splitSearchFirstFit");
                }
                if (args[1].equals("ff")) {
                    //use first fit
					client.firstFit();
                    System.out.println("first fit");
                }
            }
                
        }
        else if(args.length >= 1) {
            System.out.println("Client: option requires an argument -- 'a'\n" +
                    "Option -a requires an argument.\n" +
                    "Usage:\n" +
                    "       java Client [-a algo_name(ff/bf/wf)]");
        }
        else {
            client.allToLargest();
            System.out.println("allto");
        }
        client.close();
    }
}

class Server {

    private String type;
    private int limit;
    private int bootupTime;
    private float rate;
    private int coreCount;
    private int memory;
    private int disk;

    public Server(String type, int limit, int bootupTime, float rate, int coreCount, int memory, int disk) {
        this.type = type;
        this.limit = limit;
        this.bootupTime = bootupTime;
        this.rate = rate;
        this.coreCount = coreCount;
        this.memory = memory;
        this.disk = disk;
    }

    @Override
    public String toString() {
        return "<" + type + ", " + limit + ", " + bootupTime + ", " + rate + ", "
                + coreCount + ", " + memory + ", " + disk + ">";
    }

    public String getType()
    {
        return this.type;
    }

    public int getLimit()
    {
        return this.limit;
    }

    public int getBootupTime()
    {
        return this.bootupTime;
    }

    public float getRate()
    {
        return this.rate;
    }

    public int getCoreCount()
    {
        return this.coreCount;
    }

    public int getMemorString()
    {
        return this.memory;
    }

    public int getDisk()
    {
        return this.disk;
    }
}
